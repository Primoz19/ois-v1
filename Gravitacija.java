import java.util.Scanner ;

class Gravitacija {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in) ;
        
        System.out.println("OIS je zakon") ;
        
        double C = 6.674 * Math.pow(10, -11) ; // gravitacijska konstanta
        double M = 5.972 * Math.pow(10, 24) ; // masa zemlje
        double r = 6.371 * Math.pow(10, 6) ; // polmer zemlje
        double v = sc.nextDouble() * 1000 ;
        double pospesek = (C * M) / Math.pow(r + v, 2) ;
        
        System.out.println(pospesek) ;
        
    }
}